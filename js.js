$(document).ready(function(){

    let orderList = [];
    let productDetails = {
    "Coke": 20.00
    , "Puding" : 5.00
    };

    $(document).on("click", ".product", function(){
        let product = $(this).attr('ID');
        let unitPrice = productDetails[product];
        let indexOfProduct = orderList.findIndex((obj) => {
            if (obj.product_name === product) {
                return true;
            }
            return false;
        });
        if(indexOfProduct >= 0){
            orderList[indexOfProduct].qty = orderList[indexOfProduct].qty + 1
            orderList[indexOfProduct].total_amount = orderList[indexOfProduct].qty * unitPrice;
        }else{
            orderList.push({product_name: product, qty: 1, unit_price: unitPrice, total_amount: unitPrice});
        }
        loadOrders();
    });

loadOrders = () => {
    let tbody = "";
    let countOrders = orderList.length;

    if(countOrders > 0) {
        for(prod of orderList){
            tbody += "<tr>";
            tbody += "<td><input type = 'checkbox'></td>";
            tbody += "<td>"+ prod['product_name'] +"</td>";
            tbody += "<td align = 'right'>"+ prod['qty'] +"</td>";
            tbody += "<td align = 'right'>"+ prod['unit_price']  +"</td>";
            tbody += "<td align = 'right'>"+ prod['total_amount'] +"</td>";
            tbody += "</tr>";
        }
    } else {
        tbody += "<tr>";
        tbody += "<td colspan = '4' align = 'center'>No Available Data</td>";
        tbody += "</tr>";
    }

    let totalAmount = 0;
    for (total_amount of orderList) totalAmount += total_amount['total_amount']
    tbody += "<tfooter>";
    tbody += "<tr>";
    tbody += "<td colspan = '4' align = 'right'><b>Total Amount</b></td>";
    tbody += "<td align = 'right'>"+totalAmount+"</td>";
    tbody += "</tr>";
    tbody += "</tfooter>";


$(".table tbody").html(tbody);
}
loadOrders();

});